#pragma once
#include "IFStatic.h"

class CIFButton : public CIFStatic
{
public:
	CIFButton(void);
	virtual ~CIFButton(void);
	
	char pad_0x0380[0x1C]; //0x0380
	char* m_disabledPath; //0x039C 
	char pad_0x03A0[0x18]; //0x03A0
	char* m_pressedPath; //0x03B8 
	char pad_0x03BC[0x18]; //0x03BC
};

